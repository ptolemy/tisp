"use strict"

// fundamental types
type Symbols = string;
type Figure = number; 
type List = Exp[];
type Exp = Atom | List | ((args:List) => Exp);
type Atom = Symbols | Figure;

interface Diverge<T> {tag:'diverge', reason: T};
interface Converge<T> {tag:'converge', reason: T};

type Result<D, C> =  Diverge<D> | Converge<C>;

interface Env {
    [key:Symbols]: Exp,
};

function tokenize(chars:string):string[]{
    // "Convert a string of characters into a list of tokens."
    return chars.replace(/\(/g, " ( ").replace(/\)/g, " ) ").split(" ").filter((c) => c !== "");
}

function parse(program:string):Exp {
    return read_from_tokens(tokenize(program));
}

function read_from_tokens(tokens: string[]):Exp {
    const token = tokens.shift();
    if (token === '(') {
        const l:List = [];
        while (tokens[0] !== ")") {
            l.push(read_from_tokens(tokens));
        }
        tokens.shift();
        return l;
    } else if (token === ')'){
        throw new Error("unexpected ')'");
    } else {
        if (typeof(token) !== "string") throw new Error('token is undefined');
        return atom(token);
    };
}

function atom(token:Symbols): Atom {
    const new_atom:Figure = Number(token);
    if (isNaN(new_atom)) {
        return token;
    } else {
        return new_atom;
    };
}

function standard_env(): Env {
    const env:Env = {
        '+': (args:List):Exp => {
                return parse_list_of_numbers(args).reduce((a, b) => a + b);  
            },
        '-': (args:List):Exp => {
                const nums:Figure[] = parse_list_of_numbers(args);
                const first = nums.shift();
                
                if (typeof(first) !== 'number') throw new Error('expected a first element in list to be number');

                const sum_rest = nums.reduce((a, b) => a + b);
                return Number(first - sum_rest);
            }
    };
    return env;
}

function parse_list_of_numbers(args:List):Figure[] {
    return args.map((arg) => parse_single_number(arg));
}

function parse_single_number(exp: Exp):Figure {
    const new_num:Figure = Number(exp);
    if (isNaN(new_num)) throw new Error('expected a number');
    
    return new_num;
}


function evaluate(x:Exp, env:Env=global_env):Exp {
    if(typeof(x) === 'string'){
        if (typeof(env[x]) === 'undefined') throw new Error('unexpected symbol');
        return env[x];
    } else if (typeof(x) === 'number'){
        return x; 
    } else if (typeof(x) === 'function'){ 
        throw new Error("unexpected form");
    } else if (Array.isArray(x)){
        const first_form = x.shift();
        if (typeof(first_form) === 'undefined') throw new Error('expected non-empty list');
        const arg_forms = x;
        const first_eval = evaluate(first_form);

        if (typeof(first_eval) === 'function') {
            const args_eval:List = arg_forms.map((x) => evaluate(x));
            return first_eval(args_eval);
        } else {
            throw new Error('first form must be a function');
        }
    } else {
        throw new Error("unexpected form");
    }
}

function repl():Promise<void> {
    // example of a valid input: "(+ 10 5 (- 10 3 3))"
    while(true) {
        const program = prompt("tisp> ");
        if (typeof(program) === 'string') {
            const parsed_program:List = Object.values(parse(program));
            console.log(evaluate(parsed_program));    
        }
        else {
            throw new Error('program entered is null');
        }
    }
}

const global_env:Env = standard_env();
repl();