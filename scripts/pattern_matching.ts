"use strict"

interface Diverge<T> {tag:'diverge', reason: T};
interface Converge<T> {tag:'converge', reason: T};

type Result<D, C> =  Diverge<D> | Converge<C>;

// https://www.geeksforgeeks.org/error-handling-with-the-either-type-in-typescript/

// types for div and con are of callback type
// parameter for the callback type is required but unused
function match<R, D, C>(
    input:Result<D, C>,
    diverge:(div:D) => R,
    converge:(con:C) => R
){
    switch(input.tag){
        case 'diverge':
            return diverge(input.reason);
        case 'converge':
            return converge(input.reason);
    }
}


const error: Diverge<Error> = {tag:'diverge', reason: new Error('error')};
const expect: Converge<string> = {tag:'converge', reason: 'success'};

const asExpected = (cond:boolean):Result<Error, string> => { 
    return cond ? expect: error;
}

const result = match(
    asExpected(false), // try toggling true and false to see how pattern works
    (err) => err.message,
    (log) => log,
);

console.log('result: ', result);