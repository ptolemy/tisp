# tisp

## lisp for deno

### Referenced
Lisp in Python
- http://norvig.com/lispy.html

Lisp in Rust
- https://stopa.io/post/222

Pattern matching
- https://blog.logrocket.com/pattern-matching-and-type-safety-in-typescript-1da1231a2e34/
- https://gist.github.com/sallar/b66467428a9015711509f70f40e4d233
